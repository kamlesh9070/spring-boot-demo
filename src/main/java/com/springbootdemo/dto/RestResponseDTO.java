package com.springbootdemo.dto;

import java.io.Serializable;

public class RestResponseDTO implements Serializable {

	private String responseMessage;
	private String responseCode;
	private transient Object responseData;
	public String getResponseMessage() {
		return responseMessage;
	}
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	public Object getResponseData() {
		return responseData;
	}
	public void setResponseData(Object responseData) {
		this.responseData = responseData;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((responseCode == null) ? 0 : responseCode.hashCode());
		result = prime * result + ((responseData == null) ? 0 : responseData.hashCode());
		result = prime * result + ((responseMessage == null) ? 0 : responseMessage.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		RestResponseDTO other = (RestResponseDTO) obj;
		if (responseCode == null) {
			if (other.responseCode != null) {
				return false;
			}
		} else if (!responseCode.equals(other.responseCode)) {
			return false;
		}
		if (responseData == null) {
			if (other.responseData != null) {
				return false;
			}
		} else if (!responseData.equals(other.responseData)) {
			return false;
		}
		if (responseMessage == null) {
			if (other.responseMessage != null) {
				return false;
			}
		} else if (!responseMessage.equals(other.responseMessage)) {
			return false;
		}
		return true;
	}
	@Override
	public String toString() {
		return "RestResponseDTO [responseMessage=" + responseMessage + ", responseCode=" + responseCode
				+ ", responseData=" + responseData + "]";
	}
	
}
