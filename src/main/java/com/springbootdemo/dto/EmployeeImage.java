package com.springbootdemo.dto;

import com.springbootdemo.bean.IResponseEntity;

public class EmployeeImage  implements IResponseEntity {

	Long employeeId;
	byte[] image;
	public Long getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}
	
	public byte[] getImage() {
		return image;
	}
	public void setImage(byte[] image) {
		this.image = image;
	}
	@Override
	public String toString() {
		return "EmployeeIds [employeeId=" + employeeId + "]";
	}
	
	
	
}

