package com.springbootdemo.dto;


import java.io.Serializable;

public class ErrorResponseDTO implements Serializable {

	private final String errorCode;
	
	private final Object errorValue;

	public ErrorResponseDTO(String errorCode) {
		super();
		this.errorCode = errorCode;
		this.errorValue = null;
	}
	
	public ErrorResponseDTO(String errorCode,Object errorValue) {
		super();
		this.errorCode = errorCode;
		this.errorValue = errorValue;
	}

	public String getErrorCode() {
		return errorCode;
	}
	
	public Object getErrorValue() {
		return errorValue;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((errorCode == null) ? 0 : errorCode.hashCode());
		result = prime * result + ((errorValue == null) ? 0 : errorValue.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ErrorResponseDTO other = (ErrorResponseDTO) obj;
		if (errorCode == null) {
			if (other.errorCode != null)
				return false;
		} else if (!errorCode.equals(other.errorCode))
			return false;
		if (errorValue == null) {
			if (other.errorValue != null)
				return false;
		} else if (!errorValue.equals(other.errorValue))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ErrorResponseDTO [errorCode=" + errorCode + ", errorValue=" + errorValue + "]";
	}

}