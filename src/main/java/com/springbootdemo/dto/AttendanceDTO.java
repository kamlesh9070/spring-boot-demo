package com.springbootdemo.dto;

import java.util.Date;

import com.springbootdemo.bean.IResponseEntity;

public class AttendanceDTO implements IResponseEntity {

	private static final long serialVersionUID = -2090348189177028983L;
	private Long employeeId;
	private String action;
	private Date swipeTime;
	private String location;
	private byte[] image;
	public Long getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public Date getSwipeTime() {
		return swipeTime;
	}
	public void setSwipeTime(Date swipeTime) {
		this.swipeTime = swipeTime;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	
	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}
	@Override
	public String toString() {
		return "AttendanceDTO [employeeId=" + employeeId + ", action=" + action + ", swipeTime=" + swipeTime
				+ ", location=" + location + "]";
	}	
}
