package com.springbootdemo.dto;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.springbootdemo.bean.IResponseEntity;

@Entity
public class EmployeeIds implements IResponseEntity {

	@Id
	Long employeeId;
	public Long getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}
	@Override
	public String toString() {
		return "EmployeeIds [employeeId=" + employeeId + "]";
	}
	
}
