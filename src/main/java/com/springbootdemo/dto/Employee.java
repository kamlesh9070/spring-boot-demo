package com.springbootdemo.dto;


import javax.persistence.Entity;
import javax.persistence.Id;

import com.springbootdemo.bean.IResponseEntity;

@Entity
public class Employee implements IResponseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private Long employeeId;
    private String name;
	private String location;
	private String imei;
    private byte[] image;
        
	public String getImei() {
		return imei;
	}
	public void setImei(String imei) {
		this.imei = imei;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public Long getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}
	public byte[] getImage() {
		return image;
	}
	public void setImage(byte[] image) {
		this.image = image;
	}
	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}
	
	@Override
	public String toString() {
		return "Employee [employeeId=" + employeeId + ", name=" + name + ", location=" + location + ", imei=" + imei
				+ ", image=" + image + "]";
	}		  
}
