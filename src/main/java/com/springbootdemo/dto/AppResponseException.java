package com.springbootdemo.dto;


import com.springbootdemo.response.AppResponse;

public class AppResponseException extends RuntimeException {

	private final AppResponse<?> appResponse;
	public AppResponseException(AppResponse<?> appResponse) {
		this.appResponse = appResponse;
	}
	public AppResponse<?> getAppResponse() {
		return appResponse;
	}
}