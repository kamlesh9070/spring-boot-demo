package com.springbootdemo.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.springbootdemo.dto.Attendance;
import com.springbootdemo.dto.AttendanceDTO;

@Component
public class AttendanceDTOToAttendance implements Converter<AttendanceDTO, Attendance> {

	@Override
	public Attendance convert(AttendanceDTO arg0) {
		Attendance attendance = new Attendance();
		attendance.setEmployeeId(arg0.getEmployeeId());
		String action = arg0.getAction();
		if(action != null) {
			if(("IN").equalsIgnoreCase(action)) {
				attendance.setInTime(arg0.getSwipeTime());
				attendance.setAttendanceDate(arg0.getSwipeTime());
			} else if(("OUT").equalsIgnoreCase(action)) {
				attendance.setOutTime(arg0.getSwipeTime());
				attendance.setAttendanceDate(arg0.getSwipeTime());
			} else {
				System.out.println("Invalid action");
				return null;
			}
		} else {
			System.out.println("Action not found in request");
			return null;
		}
		
		attendance.setLocation(arg0.getLocation());
		return attendance;
	}
}
