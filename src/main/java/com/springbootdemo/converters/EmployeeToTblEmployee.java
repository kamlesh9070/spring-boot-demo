package com.springbootdemo.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.springbootdemo.domain.TblEmployee;
import com.springbootdemo.dto.Employee;

@Component
public class EmployeeToTblEmployee implements Converter<Employee, TblEmployee> {

	@Override
    public TblEmployee convert(Employee employee) {
		TblEmployee tblemployee = new TblEmployee();
		tblemployee.setEmployeeId(employee.getEmployeeId());
		tblemployee.setName(employee.getName());
		tblemployee.setImei(employee.getImei());
		tblemployee.setImage(employee.getImage());
        return tblemployee;
    }
}
