package com.springbootdemo.response;


import java.util.Collection;

import com.springbootdemo.bean.IResponseEntity;
import com.springbootdemo.utility.ApplicationConstants;

public class AppResponse<T extends IResponseEntity> {
	
	private int responseCode;
	private String responseMessage;
	private Object errorValue;
	private String errorMessage;
	private T payload;
	private Collection<T> payloadList;
	
	
	public int getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}
	public String getResponseMessage() {
		return responseMessage;
	}
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
	public T getPayload() {
		return payload;
	}
	public void setPayload(T payload) {
		this.payload = payload;
	}
	
	public boolean isSuccess(){
		return (this.responseCode == ApplicationConstants.SUCCESS);
	}
	public Collection<T> getPayloadList() {
		return payloadList;
	}
	public void setPayloadList(Collection<T> payloadList) {
		this.payloadList = payloadList;
	}
	public Object getErrorValue() {
		return errorValue;
	}
	public void setErrorValue(Object errorValue) {
		this.errorValue = errorValue;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	
	

}
