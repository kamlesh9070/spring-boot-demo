package com.springbootdemo.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.springbootdemo.dto.Employee;

public interface EmployeeRepository extends CrudRepository<Employee, Long> {
	List<Employee> findByEmployeeId(Long employeeId);
}
