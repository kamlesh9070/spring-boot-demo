package com.springbootdemo.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.springbootdemo.dto.Attendance;

public interface AttendanceRepository extends CrudRepository<Attendance, Long> {
	List<Attendance> findByEmployeeId(Long employeeId);
	List<Attendance> findByAttendanceDate(Date attendanceDate);
}
