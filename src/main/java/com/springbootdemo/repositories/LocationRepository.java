package com.springbootdemo.repositories;

import org.springframework.data.repository.CrudRepository;

import com.springbootdemo.dto.Location;

public interface LocationRepository extends CrudRepository<Location, Long> {

}
