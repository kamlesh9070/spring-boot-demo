package com.springbootdemo.repositories;

import org.springframework.data.repository.CrudRepository;

import com.springbootdemo.dto.EmployeeIds;

public interface EmployeeIdRepository extends CrudRepository<EmployeeIds, Long> {

}
