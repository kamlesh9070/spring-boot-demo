package com.springbootdemo.controllers.rest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springbootdemo.dto.EmployeeIds;
import com.springbootdemo.helper.base.ICRUDHelper;
import com.springbootdemo.helper.impl.EmployeeIdHelper;

@RestController
@RequestMapping(value = "/webui/api/employeeIds")
public class EmployeeIdController extends CRUDController<EmployeeIds, EmployeeIdHelper> {

	@Autowired
	private EmployeeIdHelper employeeIdHelper;

	protected ICRUDHelper<EmployeeIds> getAssociatedHelper() {
		return employeeIdHelper;
	}
	@Override
	public String getModuleName() {
		return "[EmployeeIdController]";
	}
}