package com.springbootdemo.controllers.rest;


import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.springbootdemo.bean.IResponseEntity;
import com.springbootdemo.dto.AppResponseException;
import com.springbootdemo.helper.base.ICRUDHelper;

public abstract class CRUDController<T extends IResponseEntity, H extends ICRUDHelper<T>> extends BaseController {

	public abstract String getModuleName();
	
	@PostMapping
	public @ResponseBody ResponseEntity<?> create(@RequestBody T t) throws AppResponseException {
		final String SUBMODULE=getModuleName()+"[create] ";
		System.out.println(SUBMODULE+"called with #entity:"+t);
		return this.getResponseEntity(this.getAssociatedHelper().create(t));
	}

	@GetMapping
	public @ResponseBody ResponseEntity<?> getAll() throws AppResponseException {
		final String SUBMODULE=getModuleName()+"[getAll] ";
		return this.getResponseEntity(this.getAssociatedHelper().getAll());
	}

	/**
	 * Here Identity is not required but have kept it for easy debugging
	 * 
	 * @param identity
	 * @param t
	 * @return
	 * @throws AppResponseException 
	 */
	@PutMapping(value = "/{identity}")
	public @ResponseBody ResponseEntity<?> update(@PathVariable(value = "identity", required = true) String identity,
			@RequestBody T t) throws AppResponseException {
		final String SUBMODULE=getModuleName()+"[update] ";
		System.out.println(SUBMODULE+"called with #entity:"+t);
		return this.getResponseEntity(this.getAssociatedHelper().update(t));
	}

	@DeleteMapping(value = "/{identity}")
	public @ResponseBody ResponseEntity<?> delete(@PathVariable(value = "identity", required = true) String identity) throws AppResponseException {
		final String SUBMODULE=getModuleName()+"[delete] ";
		return this.getResponseEntity(this.getAssociatedHelper().delete(identity));		
	}

	protected abstract ICRUDHelper<T> getAssociatedHelper();

}