package com.springbootdemo.controllers.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.springbootdemo.bean.IResponseEntity;
import com.springbootdemo.dto.RestResponseDTO;
import com.springbootdemo.response.AppResponse;
import com.springbootdemo.utility.ApplicationConstants;

public abstract class BaseController extends ResponseEntityExceptionHandler {

	public <S extends IResponseEntity> ResponseEntity<?> getResponseEntity(AppResponse<S> appResponse) {

		if (appResponse == null) {
			return new ResponseEntity<>(ApplicationConstants.INTERNAL_SERVER_ERROR_MSG_CODE,HttpStatus.INTERNAL_SERVER_ERROR);
		} else if (appResponse.isSuccess()) {
			RestResponseDTO restResponseDTO = new RestResponseDTO();
			restResponseDTO.setResponseCode("1");
			restResponseDTO.setResponseMessage(ApplicationConstants.SUCCESS_MSG_CODE);
			if (appResponse.getPayload() == null && appResponse.getPayloadList() == null) {
				return new ResponseEntity<>(restResponseDTO, HttpStatus.OK);
			} else if (appResponse.getPayload() != null) {
				restResponseDTO.setResponseData(appResponse.getPayload());
				return new ResponseEntity<>(restResponseDTO, HttpStatus.OK);
			} else {
				restResponseDTO.setResponseData(appResponse.getPayloadList());
				return new ResponseEntity<>(restResponseDTO, HttpStatus.OK);
			}
		} else {
			RestResponseDTO restResponseDTO = new RestResponseDTO();
			restResponseDTO.setResponseCode(String.valueOf(appResponse.getResponseCode()));
			restResponseDTO.setResponseMessage(appResponse.getResponseMessage());
			// return new ResponseEntity<>(new
			// ErrorResponseDTO(appResponse.getResponseMessage(),appResponse.getErrorValue()),getHttpStatusCode(appResponse.getResponseCode()));
			return new ResponseEntity<>(restResponseDTO, getHttpStatusCode(appResponse.getResponseCode()));
		}

	}

	public HttpStatus getHttpStatusCode(int appResponseCode) {

		HttpStatus httpStatus;

		switch (appResponseCode) {

			case ApplicationConstants.ENTITY_EXISTS:
				httpStatus = HttpStatus.CONFLICT;
				break;
	
			case ApplicationConstants.SUCCESS:
				httpStatus = HttpStatus.OK;
				break;
	
			case ApplicationConstants.ENTITY_NOT_EXISTS:
			case ApplicationConstants.UNPROCESSABLE_ENTITY:
				httpStatus = HttpStatus.UNPROCESSABLE_ENTITY;
				break;
	
			case ApplicationConstants.ERROR:
			default:
				httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;

		}
		return httpStatus;
	}
}
