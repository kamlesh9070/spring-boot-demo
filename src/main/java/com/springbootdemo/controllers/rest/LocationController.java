package com.springbootdemo.controllers.rest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springbootdemo.dto.Location;
import com.springbootdemo.helper.base.ICRUDHelper;
import com.springbootdemo.helper.impl.LocationHelper;

@RestController
@RequestMapping(value = "/webui/api/locations")
public class LocationController extends CRUDController<Location, LocationHelper> {

	@Autowired
	private LocationHelper locationHelper;

	protected ICRUDHelper<Location> getAssociatedHelper() {
		return locationHelper;
	}
	@Override
	public String getModuleName() {
		return "[LocationController]";
	}
}