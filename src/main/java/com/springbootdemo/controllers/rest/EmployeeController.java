package com.springbootdemo.controllers.rest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.springbootdemo.dto.AppResponseException;
import com.springbootdemo.dto.Employee;
import com.springbootdemo.dto.EmployeeImage;
import com.springbootdemo.helper.base.ICRUDHelper;
import com.springbootdemo.helper.impl.EmployeeHelper;

@RestController
@RequestMapping(value = "/webui/api/employees")
public class EmployeeController extends CRUDController<Employee, EmployeeHelper> {

	@Autowired
	private EmployeeHelper employeeHelper;

	protected ICRUDHelper<Employee> getAssociatedHelper() {
		return employeeHelper;
	}
	@Override
	public String getModuleName() {
		return "[EmployeeController]";
	}
	
	@PostMapping("/uploadImage")
	public @ResponseBody ResponseEntity<EmployeeImage> uploadImage(@RequestBody EmployeeImage t) throws AppResponseException {
		return (ResponseEntity<EmployeeImage>) getResponseEntity(employeeHelper.uploadImage(t));
	}
}