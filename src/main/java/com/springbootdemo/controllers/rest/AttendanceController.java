package com.springbootdemo.controllers.rest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.springbootdemo.dto.AppResponseException;
import com.springbootdemo.dto.Attendance;
import com.springbootdemo.dto.AttendanceDTO;
import com.springbootdemo.helper.base.ICRUDHelper;
import com.springbootdemo.helper.impl.AttendanceHelper;

@RestController
@RequestMapping(value = "/webui/api/attendance")
public class AttendanceController extends CRUDController<AttendanceDTO, AttendanceHelper> {

	@Autowired
	private AttendanceHelper attendanceHelper;

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/byempid/{employeeid}", method = { RequestMethod.POST, RequestMethod.PUT }, consumes = "application/json")
	public @ResponseBody ResponseEntity<Attendance> getAttendanceByEmployeeId(@PathVariable("employeeid") final Long employeeid) throws AppResponseException {
		return (ResponseEntity<Attendance>) getResponseEntity(attendanceHelper.findByEmployeeId(employeeid));
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/bydate/{attendancedate}", method = { RequestMethod.POST, RequestMethod.PUT }, consumes = "application/json")
	public @ResponseBody ResponseEntity<Attendance> getAttendanceByDate(@PathVariable("attendancedate") final Long attendancedate) throws AppResponseException {
		return (ResponseEntity<Attendance>) getResponseEntity(attendanceHelper.findByDate(attendancedate));
	}
	
	@GetMapping("/generateCSV")
    public void generateCSV() {
        attendanceHelper.generateCSV(); 
    }
	
	protected ICRUDHelper<AttendanceDTO> getAssociatedHelper() {
		return attendanceHelper;
	}
	@Override
	public String getModuleName() {
		return "[AttendanceController]";
	}
}