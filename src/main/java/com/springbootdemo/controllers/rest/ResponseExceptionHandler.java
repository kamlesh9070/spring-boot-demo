package com.springbootdemo.controllers.rest;


import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.springbootdemo.dto.AppResponseException;

@ControllerAdvice
public class ResponseExceptionHandler extends BaseController {

	@ExceptionHandler(AppResponseException.class)
	public @ResponseBody ResponseEntity<?> handleApplicationResponseException(AppResponseException ex) {
		return getResponseEntity(ex.getAppResponse());
	}
}
