package com.springbootdemo;

import org.bytedeco.javacpp.Loader;
import org.bytedeco.javacpp.opencv_java;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootPostgreSQLApplication {

	public static void main(String[] args) {
		String sysPath = System.getProperty("user.dir");
		System.out.println(sysPath);
		Loader.load(opencv_java.class);
		System.out.println("Library loaded!!");
		SpringApplication.run(SpringBootPostgreSQLApplication.class, args);
	}
}
