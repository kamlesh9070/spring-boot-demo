package com.springbootdemo.helper.base;

import javax.validation.Valid;

import com.springbootdemo.bean.IResponseEntity;
import com.springbootdemo.dto.AppResponseException;
import com.springbootdemo.response.AppResponse;

public interface ICRUDHelper<T extends IResponseEntity> {

	public AppResponse<T> create(@Valid T t) throws AppResponseException;

	public AppResponse<T> update(T t) throws AppResponseException;

	public AppResponse<T> delete(String entityName) throws AppResponseException;

	public AppResponse<T> getAll() throws AppResponseException;

}
