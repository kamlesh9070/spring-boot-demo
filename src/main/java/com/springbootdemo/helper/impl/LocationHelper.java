package com.springbootdemo.helper.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import com.springbootdemo.dto.Location;
import com.springbootdemo.repositories.LocationRepository;

@Service("locationHelper")
public class LocationHelper extends CRUDHelper<Location, Long> {

	private final String MODULE = "[LocationHelper]";
	@Autowired
	private LocationRepository locationRepository;

	@Override
	public String getModuleName() {
		return MODULE;
	}

	/*
	 * @Override public AppResponse<Location> create(@Valid Location t) throws
	 * AppResponseException { t.setLocationDate(new Date());
	 * getAssociateRepository().save(t); return getAppResponse(t); }
	 */

	@Override
	protected CrudRepository<Location, Long> getAssociateRepository() {
		return locationRepository;
	}

}