package com.springbootdemo.helper.impl;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.data.repository.CrudRepository;

import com.springbootdemo.bean.IResponseEntity;
import com.springbootdemo.dto.AppResponseException;
import com.springbootdemo.helper.base.ICRUDHelper;
import com.springbootdemo.response.AppResponse;
import com.springbootdemo.utility.ApplicationConstants;

public abstract class CRUDHelper<T extends IResponseEntity, ID> extends BaseHelper implements ICRUDHelper<T> {

	public abstract String getModuleName();

	@Override
	public AppResponse<T> create(@Valid T t) throws AppResponseException {
		getAssociateRepository().save(t);
		return getAppResponse(t);
	}

	@Override
	public AppResponse<T> update(T t) throws AppResponseException {
		getAssociateRepository().save(t);
		return getAppResponse(t);
	}

	@Override
	public AppResponse<T> delete(String entityName) throws AppResponseException {
		getAssociateRepository().deleteById((ID) (entityName));
		return getAppResponseByCode(ApplicationConstants.SUCCESS);
	}

	@Override
	public AppResponse<T> getAll() throws AppResponseException {
		List<T> entities = new ArrayList<>();
		getAssociateRepository().findAll().forEach(entities::add);
		return getAppResponse(entities);
	}

	protected abstract CrudRepository<T, ID> getAssociateRepository();

}