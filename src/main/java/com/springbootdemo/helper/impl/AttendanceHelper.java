package com.springbootdemo.helper.impl;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.validation.Valid;

import org.opencv.core.Mat;
import org.opencv.core.MatOfInt;
import org.opencv.face.EigenFaceRecognizer;
import org.opencv.imgcodecs.Imgcodecs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.springbootdemo.converters.AttendanceDTOToAttendance;
import com.springbootdemo.dto.AppResponseException;
import com.springbootdemo.dto.Attendance;
import com.springbootdemo.dto.AttendanceDTO;
import com.springbootdemo.dto.Employee;
import com.springbootdemo.helper.base.ICRUDHelper;
import com.springbootdemo.repositories.AttendanceRepository;
import com.springbootdemo.repositories.EmployeeRepository;
import com.springbootdemo.response.AppResponse;
import com.springbootdemo.utility.ApplicationConstants;


@Service("attendanceHelper")
public class AttendanceHelper extends BaseHelper implements ICRUDHelper<AttendanceDTO> {

	private final String MODULE = "[AttendanceHelper]";
	@Autowired
	private AttendanceRepository attendanceRepository;
	@Autowired
	private EmployeeRepository employeeRepository;
	
	String sysPath = System.getProperty("user.dir");
	
	@Override 
	public AppResponse<AttendanceDTO> create(@Valid AttendanceDTO t) throws
	AppResponseException {
		if (t.getImage() == null) {
			throw new AppResponseException(getAppResponse("Image is null", ApplicationConstants.UNPROCESSABLE_ENTITY));
		}
		Double confidence = new Double(-100);
		int outLabel = verifyImage(t,confidence);
		if (outLabel != 0) {
			//throw new AppResponseException(getAppResponse("Invalid Person Output:" + outLabel + "Per:" + confidence.longValue() +" Input:" + t.getEmployeeId(), ApplicationConstants.UNPROCESSABLE_ENTITY));
			throw new AppResponseException(getAppResponse("Opps!! Your profile does not match. Please swipe in/out with valid User", ApplicationConstants.UNPROCESSABLE_ENTITY));
		}
		
		String msg = "Confidence:" + confidence.longValue();
		List<Attendance> attendances = attendanceRepository.findByEmployeeId(t.getEmployeeId());
		Attendance attendance;
		if(attendances.size() > 0) {
			attendance = attendances.get(0);
			String action = t.getAction();
			if(action != null) {
				if(("IN").equalsIgnoreCase(action)) {
					attendance.setInTime(t.getSwipeTime());
					attendance.setAttendanceDate(t.getSwipeTime());
				} else if(("OUT").equalsIgnoreCase(action)) {
					attendance.setOutTime(t.getSwipeTime());
					attendance.setAttendanceDate(t.getSwipeTime());
				} else {
					System.out.println("Invalid action");
					return getAppResponse(ApplicationConstants.ERROR_MSG_CODE, ApplicationConstants.ERROR, t);
				}
			} else {
				System.out.println("Action not found in request");
				return getAppResponse(ApplicationConstants.ERROR_MSG_CODE, ApplicationConstants.ERROR, t);
			}
		} else {
			AttendanceDTOToAttendance toAttendance = new AttendanceDTOToAttendance();
			attendance = toAttendance.convert(t);
		}
		List<Employee> employees = employeeRepository.findByEmployeeId(t.getEmployeeId());
		if (employees != null && !employees.isEmpty())
			attendance.setName(employees.get(0).getName());
		attendanceRepository.save(attendance);
		return getAppResponse(t); 
	}
	
	@Override
	public AppResponse<AttendanceDTO> update(AttendanceDTO t) throws AppResponseException {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public AppResponse<AttendanceDTO> delete(String entityName) throws AppResponseException {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public AppResponse<AttendanceDTO> getAll() throws AppResponseException {
		// TODO Auto-generated method stub
		return null;
	}
	
	public AppResponse<Attendance> findByEmployeeId(Long employeeId) throws
	AppResponseException { 
		return getAppResponse(attendanceRepository.findByEmployeeId(employeeId));
	}	

	private int verifyImage(AttendanceDTO attendanceDTO, Double confidence) {
		ArrayList<Mat> images = new ArrayList<>();
		ArrayList<Integer> labels = new ArrayList<>();
		String sysPath = System.getProperty("user.dir");
		String trainingDir = sysPath + File.separator + "images";
		readImagesMulti(trainingDir, images, labels);
		MatOfInt labelsMat = new MatOfInt();
		labelsMat.fromList(labels);
		EigenFaceRecognizer efr = EigenFaceRecognizer.create();
		System.out.println("Starting training...");
		efr.train(images, labelsMat);
		File temp = null;
		try {
			temp = File.createTempFile("tempfile", ".jpg");
			FileOutputStream fos = new FileOutputStream(temp);
			fos.write(attendanceDTO.getImage());
		} catch (Exception e) {
			e.printStackTrace();
		}
		Mat readImage = Imgcodecs.imread(temp.getAbsolutePath(), 0);
		int[] outLabel = new int[1];
		double[] outConf = new double[1];
		System.out.println("Starting Prediction...");
		efr.predict(readImage, outLabel, outConf);
		confidence = outConf[0];
		System.out.println("Input EmpId:" + attendanceDTO.getEmployeeId() + " Output EmpID:" + outLabel[0] + "Match:" + outConf[0]);
		if (outLabel[0] == attendanceDTO.getEmployeeId())
			return 0;
		else
			return outLabel[0];
	}

	private static void readImages(String trainingDir, ArrayList<Mat> images, ArrayList<Integer> labels) {
		File root = new File(trainingDir);
		FilenameFilter imgFilter = new FilenameFilter() {
			public boolean accept(File dir, String name) {
				name = name.toLowerCase();
				return name.endsWith(".jpg") || name.endsWith(".pgm") || name.endsWith(".png");
			}
		};
		File[] imageFiles = root.listFiles(imgFilter);
		for (File imageFile : imageFiles) {
			Mat readImage = Imgcodecs.imread(imageFile.getAbsolutePath(), 0);
			images.add(readImage);
			labels.add(Integer.parseInt(imageFile.getName().split("\\.")[0]));
		}
	}
	
	private static void readImagesMulti(String trainingDir, ArrayList<Mat> images, ArrayList<Integer> labels) {
		File root = new File(trainingDir);
		FilenameFilter imgFilter = new FilenameFilter() {
			public boolean accept(File dir, String name) {
				name = name.toLowerCase();
				return name.endsWith(".jpg") || name.endsWith(".pgm") || name.endsWith(".png");
			}
		};
		File[] imageFiles = root.listFiles(imgFilter);
		for (File imageFile : imageFiles) {
			Mat readImage = Imgcodecs.imread(imageFile.getAbsolutePath(), 0);
			images.add(readImage);
			labels.add(Integer.parseInt(imageFile.getName().split("\\.")[0].split("-")[0]));
		}
	}

	public AppResponse<Attendance> findByDate(Long attendanceDate) throws
	AppResponseException { 
		return getAppResponse(attendanceRepository.findByAttendanceDate(new Date(attendanceDate)));
	}
	
	public void generateCSV() {
		File temp = null;
		BufferedWriter bw = null;
		try {
			StringBuilder sb = new StringBuilder();
			sb.append("ATTDATE|EMPID|EMPLOYEENAME|INTIME|OUTTIME|LOCATION|SHIFT").append("\n");
			//sb.append("03-DEC-18|2878|Suraj Mishra|2018-12-03 09:00:00 AM |2018-12-03 06:00:00 PM |Ahmedabad|S1");
			temp = new File(sysPath + File.separator + "Codefest" + ".csv");
			//temp = new File(sysPath + File.separator + "attendance-" + new Random().nextInt() + ".csv");
			List<Attendance> attendances = new ArrayList();
			attendanceRepository.findAll().forEach(attendances :: add);
			for(Attendance attendance : attendances) {
				if(!attendance.isProcessed() && attendance.getInTime() != null && attendance.getOutTime() != null) {
					SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-YY");
					sb.append(sdf.format(attendance.getAttendanceDate())).append("|")
							.append(attendance.getEmployeeId()).append("|")
							.append(attendance.getName()).append("|");
					sdf = new SimpleDateFormat("YYYY-MM-DD hh:mm:ss aa");
					sb.append(sdf.format(attendance.getInTime())).append("|");
					sb.append(sdf.format(attendance.getOutTime())).append("|");
					sb.append(attendance.getLocation()).append("|").append("S1");
					sb.append("\n");
					attendance.setProcessed(true);
					attendanceRepository.save(attendance);
				}
			}
			FileWriter fw = new FileWriter(temp);
			bw = new BufferedWriter(fw);
			bw.write(sb.toString());
			System.out.println("File written Successfully :" + temp.getAbsolutePath());
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				bw.close();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		send(temp.getAbsolutePath());
	}
	
	public static void send (String fileName) {
        String SFTPHOST = "sftp10.successfactors.com";
        int SFTPPORT = 22;
        String SFTPUSER = "3300276T";
        String SFTPPASS = "O8IsD80IOM8k";
        String SFTPWORKINGDIR = "/incoming/Attendance";

        Session session = null;
        Channel channel = null;
        ChannelSftp channelSftp = null;
        System.out.println("preparing the host information for sftp.");
        try {
            JSch jsch = new JSch();
            session = jsch.getSession(SFTPUSER, SFTPHOST, SFTPPORT);
            session.setPassword(SFTPPASS);
            java.util.Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");
            session.setConfig(config);
            session.connect();
            System.out.println("Host connected.");
            channel = session.openChannel("sftp");
            channel.connect();
            System.out.println("sftp channel opened and connected.");
            channelSftp = (ChannelSftp) channel;
            channelSftp.cd(SFTPWORKINGDIR);
            File f = new File(fileName);
            channelSftp.put(new FileInputStream(f), f.getName());
            System.out.println("File transfered successfully to host.");
        } catch (Exception ex) {
             System.out.println("Exception found while tranfer the response.");
             ex.printStackTrace();
        }
        finally{

            channelSftp.exit();
            System.out.println("sftp Channel exited.");
            channel.disconnect();
            System.out.println("Channel disconnected.");
            session.disconnect();
            System.out.println("Host Session disconnected.");
        }
    }
}