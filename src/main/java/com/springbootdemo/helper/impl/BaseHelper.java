package com.springbootdemo.helper.impl;

import java.util.Collection;
import java.util.List;

import com.springbootdemo.bean.IResponseEntity;
import com.springbootdemo.response.AppResponse;
import com.springbootdemo.utility.ApplicationConstants;

public abstract class BaseHelper {

	public <T extends IResponseEntity> AppResponse<T> getAppResponse(String messageCode, int resCode) {
		return composeAppResponse(messageCode, resCode);
	}

	public <T extends IResponseEntity> AppResponse<T> getAppResponse(Collection<T> payload) {
		return composeAppResponse(ApplicationConstants.SUCCESS_MSG_CODE, ApplicationConstants.SUCCESS, null, payload);
	}

	public <T extends IResponseEntity> AppResponse<T> getAppResponse(T payload) {
		return composeAppResponse(ApplicationConstants.SUCCESS_MSG_CODE, ApplicationConstants.SUCCESS, payload, null);
	}

	public <T extends IResponseEntity> AppResponse<T> composeAppResponse(String messageCode, int resCode, T payload,
			Collection<T> payloadList) {
		AppResponse<T> appResponse = new AppResponse<T>();
		appResponse.setResponseMessage(messageCode);
		appResponse.setPayloadList(payloadList);
		appResponse.setResponseCode(resCode);
		appResponse.setPayload(payload);
		return appResponse;

	}

	public <T extends IResponseEntity> AppResponse<T> getErrorAppResponse(String messageCode, int resCode, Object errorValue) {
		AppResponse<T> appResponse = new AppResponse<T>();
		appResponse.setResponseMessage(messageCode);
		appResponse.setErrorValue(errorValue);
		appResponse.setErrorMessage(messageCode);
		appResponse.setResponseCode(resCode);
		return appResponse;
	}

	public <T extends IResponseEntity> AppResponse<T> composeAppResponse(String messageCode, int resCode) {
		return composeAppResponse(messageCode, resCode, null, null);
	}

	public <T extends IResponseEntity> AppResponse<T> getAppResponse(String messageCode, int resCode, List<T> payloadList) {
		return composeAppResponse(messageCode, resCode, null, payloadList);
	}

	public <T extends IResponseEntity> AppResponse<T> getAppResponse(String messageCode, int resCode, T payload) {
		return composeAppResponse(messageCode, resCode, payload, null);
	}

	public <T extends IResponseEntity> AppResponse<T> getSuccessAppResponse() {
		return getAppResponse(ApplicationConstants.SUCCESS_MSG_CODE, ApplicationConstants.SUCCESS);
	}

	public <T extends IResponseEntity> AppResponse<T> getUnknowErrorAppResponse() {
		return getAppResponse(ApplicationConstants.ERROR_MSG_CODE, ApplicationConstants.ERROR);
	}

	public <T extends IResponseEntity> AppResponse<T> getInternalErrorAppResponse() {
		return getAppResponse(ApplicationConstants.INTERNAL_SERVER_ERROR_MSG_CODE, ApplicationConstants.INTERNAL_SERVER_ERROR);
	}

	public <T extends IResponseEntity> AppResponse<T> getAppResponseByCode(int resCode) {

		switch (resCode) {
		case ApplicationConstants.SUCCESS:
			return this.getSuccessAppResponse();
		case ApplicationConstants.ERROR:
			return this.getUnknowErrorAppResponse();

		case ApplicationConstants.INTERNAL_SERVER_ERROR:
		default:
			return this.getUnknowErrorAppResponse();

		}
	}

}