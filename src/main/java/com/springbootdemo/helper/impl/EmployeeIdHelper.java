package com.springbootdemo.helper.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import com.springbootdemo.dto.AppResponseException;
import com.springbootdemo.dto.Employee;
import com.springbootdemo.dto.EmployeeIds;
import com.springbootdemo.repositories.EmployeeIdRepository;
import com.springbootdemo.repositories.EmployeeRepository;
import com.springbootdemo.response.AppResponse;
import com.springbootdemo.utility.ApplicationConstants;

@Service("employeeIdHelper")
public class EmployeeIdHelper extends CRUDHelper<EmployeeIds, Long> {

	private final String MODULE = "[EmployeeIDHelper]";
	@Autowired
	private EmployeeIdRepository employeeIdRepository;
	String sysPath = System.getProperty("user.dir");
	
	@Override
	public String getModuleName() {
		return MODULE;
	}

	@Override
	protected CrudRepository<EmployeeIds, Long> getAssociateRepository() {
		return employeeIdRepository;
	}

}