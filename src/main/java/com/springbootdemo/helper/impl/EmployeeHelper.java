package com.springbootdemo.helper.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import com.springbootdemo.dto.AppResponseException;
import com.springbootdemo.dto.Employee;
import com.springbootdemo.dto.EmployeeIds;
import com.springbootdemo.dto.EmployeeImage;
import com.springbootdemo.repositories.EmployeeIdRepository;
import com.springbootdemo.repositories.EmployeeRepository;
import com.springbootdemo.response.AppResponse;
import com.springbootdemo.utility.ApplicationConstants;

@Service("employeeHelper")
public class EmployeeHelper extends CRUDHelper<Employee, Long> {

	private final String MODULE = "[EmployeeHelper]";
	@Autowired
	private EmployeeRepository employeeRepository;
	@Autowired
	private EmployeeIdRepository employeeIdRepository;
	String sysPath = System.getProperty("user.dir");
	@Override
	public AppResponse<Employee> create(@Valid Employee t) throws AppResponseException {
		if (t.getImage() == null) {
			throw new AppResponseException(getAppResponse("Image is null", ApplicationConstants.UNPROCESSABLE_ENTITY));
		}
		List<Long> employeeIds = new ArrayList<>();
		employeeIdRepository.findAll().forEach(id -> employeeIds.add(id.getEmployeeId())); // fun with Java 8
		if(!employeeIds.contains(t.getEmployeeId())) {
			System.out.println("EmployeeId is not present in DB : " + t.getEmployeeId());
			throw new AppResponseException(getAppResponse("User is not authorized", ApplicationConstants.UNPROCESSABLE_ENTITY));
		}
		File file = new File(getFileName(t.getEmployeeId()));
		try (FileOutputStream fos = new FileOutputStream(file)) {
			fos.write(t.getImage());
		} catch (Exception e) {
			e.printStackTrace();
		}
		t.setImage(null);
		employeeRepository.save(t);
		return getAppResponse(t);
	}

	@Override
	public AppResponse<Employee> update(Employee t) throws AppResponseException {
		employeeRepository.save(t);
		return getAppResponse(t);
	}

	@Override
	public AppResponse<Employee> delete(String entityName) throws AppResponseException {
		employeeRepository.deleteById(Long.parseLong(entityName));
		return getAppResponseByCode(ApplicationConstants.SUCCESS);
	}

	@Override
	public AppResponse<Employee> getAll() throws AppResponseException {
		List<Employee> employees = new ArrayList<>();
		employeeRepository.findAll().forEach(employees::add); // fun with Java 8
		return getAppResponse(employees);
	}

	@Override
	public String getModuleName() {
		return MODULE;
	}

	@Override
	protected CrudRepository<Employee, Long> getAssociateRepository() {
		return employeeRepository;
	}

	
	public AppResponse<Employee> uploadImage(@Valid EmployeeImage employeeImage) throws AppResponseException {
		if (employeeImage.getImage() == null) {
			throw new AppResponseException(getAppResponse("Image is null", ApplicationConstants.UNPROCESSABLE_ENTITY));
		}
		File file = new File(getFileName(employeeImage.getEmployeeId()));
		try (FileOutputStream fos = new FileOutputStream(file)) {
			fos.write(employeeImage.getImage());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return getAppResponseByCode(ApplicationConstants.SUCCESS);
	}
	
	private String getFileName(Long employeeId) {
		return sysPath + File.separator + "images" + File.separator + employeeId + "-" + new Random().nextInt() + ".jpg";
	}
	
}